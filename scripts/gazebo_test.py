#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import String
from geometry_msgs.msg import Twist
import time
import math

"""TODOs:
- Gazebo map with several entries
- Automatic closing of some entries mid-test
- Lots of people walking simulation
"""


def lift(topic_name, speed, duration):
    rospy.loginfo(
        "Lifting on {} at speed {} for {} seconds".format(topic_name, speed, duration)
    )
    pub = rospy.Publisher(topic_name, Twist, queue_size=10)
    rate = rospy.Rate(10)  # 10hz
    t0 = time.time()
    leave = False
    while not rospy.is_shutdown() and not (leave):
        twist = Twist()
        t = time.time() - t0
        if t > duration:
            twist.linear.z = 0
            leave = True
        twist.linear.z = speed
        pub.publish(twist)
        rate.sleep()
    rospy.loginfo("End of lifting")
    return


def test_sinus(topic_name):
    rospy.loginfo("test_sinus on {}".format(topic_name))
    pub = rospy.Publisher(topic_name, Twist, queue_size=10)
    rate = rospy.Rate(10)  # 10hz
    t0 = time.time()
    while not rospy.is_shutdown():
        twist = Twist()
        t = time.time() - t0
        if t < 4:
            twist.linear.x = 0.2
        elif t >= 4 and t < 15:
            twist.linear.y = 0.2
        elif t >= 15 and t < 30:
            twist.linear.x = -0.2
        else:
            twist.linear.x = 0.2 * math.sin(2 * math.pi * 0.5 * t)
            twist.linear.y = 0.2 * math.sin(2 * math.pi * 0.5 * t)
            twist.angular.z = 0.5

        pub.publish(twist)
        rate.sleep()


def escape(topic_name):
    rospy.loginfo("escape on {}".format(topic_name))
    pub = rospy.Publisher(topic_name, Twist, queue_size=10)
    rate = rospy.Rate(10)  # 10hz
    t0 = time.time()
    while not rospy.is_shutdown():
        twist = Twist()
        t = time.time() - t0
        if t < 6:
            twist.linear.x = 0.2
        elif t < 21:
            twist.linear.y = 0.2
        elif t < 48:
            twist.linear.x = -0.2
        elif t < 60:
            twist.linear.y = 0.2
        elif t < 68:
            twist.linear.x = -0.2
        else:
            twist.linear.x = 0
            twist.linear.y = 0
            twist.angular.z = 0

        pub.publish(twist)
        rate.sleep()


def escape_friendly(topic_name):
    rospy.loginfo("escape on {}".format(topic_name))
    pub = rospy.Publisher(topic_name, Twist, queue_size=10)
    rate = rospy.Rate(10)  # 10hz
    t0 = time.time()
    while not rospy.is_shutdown():
        twist = Twist()
        t = time.time() - t0
        if t < 6:
            twist.linear.x = 0.2
        elif t < 11:
            twist.linear.z = 2.5
            twist.linear.y = -0.5
        elif t < 17:
            twist.linear.z = -2.5
            twist.linear.y = -0.5
        else:
            twist.linear.x = 0
            twist.linear.y = 0
            twist.angular.z = 0

        pub.publish(twist)
        rate.sleep()


if __name__ == "__main__":
    try:
        rospy.init_node("simple_gazebo_model_controller", anonymous=True)
        # lift("/cmd_vel_turtlebot3_house_friendly", 0.3, 5)
        # test_sinus("/cmd_vel_cylinder2")
        # test_sinus("/cmd_vel_cylinder")
        # test_sinus("/cmd_vel_turtlebot3_house_friendly")
        # escape("/cmd_vel_cylinder")
        escape_friendly("/cmd_vel_cylinder")
    except rospy.ROSInterruptException:
        pass
